import { sectionIds } from '@/utils/constants';
import { useEffect, useState } from 'react';

export const useScrollSpy = (ids: string[]) => {
  const [activeSection, setActiveSection] = useState<string>(sectionIds[0]);
  useEffect(() => {
    const handleScroll = () => {
      const scrollPosition = window.scrollY;
      ids.forEach((id) => {
        const element = document.getElementById(id);
        if (element) {
          const { offsetTop, offsetHeight } = element;
          if (
            scrollPosition >= offsetTop &&
            scrollPosition < offsetTop + offsetHeight
          ) {
            setActiveSection(id);
          }
        }
      });
    };

    window.addEventListener('scroll', handleScroll);

    // Clean up the event listener on component unmount
    return () => {
      window.removeEventListener('scroll', handleScroll);
    };
  }, [ids]);

  return activeSection;
};
