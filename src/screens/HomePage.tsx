'use client';
import { Sidebar } from '@/components';
import { useScrollSpy } from '@/hooks';
import { sectionIds } from '@/utils/constants';
import Link from 'next/link';
import { useEffect } from 'react';

const HomePage: React.FC = () => {
  const activeSection = useScrollSpy(sectionIds);
  useEffect(() => {
    console.log(activeSection);
  }, [activeSection]);
  return (
    <main>
      <h1>Hello World</h1>
      <Sidebar />
      <div className='grid grid-cols-3 gap-4'>
        <div>
          {sectionIds.map((id) => (
            <div key={id} id={id} className='h-screen bg-blue-100 w-full'>
              {id}
            </div>
          ))}
        </div>

        <div className='grid grid-cols-1 gap-4 fixed right-9 top-9'>
          {sectionIds.map((href) => (
            <Link
              className={href === activeSection ? 'text-red-500' : ''}
              key={href}
              href={`#${href}`}
            >
              {href}
            </Link>
          ))}
        </div>
      </div>
    </main>
  );
};

export default HomePage;
