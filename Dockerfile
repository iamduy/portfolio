FROM node:18 AS development

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm i

COPY . .

ENV NODE_ENV production

RUN npm run build
